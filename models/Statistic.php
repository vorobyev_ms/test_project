<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
use keltstr\simplehtmldom\SimpleHTMLDom as Parcer;
/**
 * Description of statistic
 *
 * @author root
 */
class Statistic extends Model {
   
    const FIRST_TR_INDEX = 3;
    
    const TD_TICKET_INDEX = 0;
    const TD_TIME_INDEX = 1;
    const TD_OPER_INDEX = 2;
    const TD_VALUE_INDEX = 13;
    const TD_BALANCEVALUE_INDEX = 4;
    
    const TD_TITLE_BUY = "buy";
    const TD_TITLE_BALANCE = "balance";
    
    public $fileName;
    
    public $htmlFile;
    
    private $pathToSave = "down";
    
    private $ext = "html";
    
    public $tmpFileName;
    
    public $byTime;
    
    public $withBalance;
    
    public $file;

    public function rules() {
       return [
            [
                "htmlFile", "file", 
                "extensions" => [$this->ext], 
                "wrongExtension" => "Неправильный формат файла",
                "checkExtensionByMimeType" => false],
           [['file','byTime','withBalance','fileName'],'safe'],
           ['htmlFile','required'],
        ];
       
    }
    
    public function downloadFile () {
        
        $this->htmlFile = UploadedFile::getInstance($this, 'fileName');
        if (empty($this->htmlFile)) {
            $this->addError('fileName',"Файл не загружен");
            return false;
        }
        if ($this->validate()) { 
            $this->tmpFileName = uniqid();
            $uploaded = $this->htmlFile->saveAs($this->getLocalPath($this->tmpFileName));
            return true;
        } else {
            $this->addError('fileName',$this->getFirstError('htmlFile'));
            return false;
        }
        
    }
    
    private function getLocalPath ($fileName) {
        return Yii::getAlias('@app')."/".$this->pathToSave."/".$fileName.'.'.$this->ext;
    }
    
    public function isExistsFile () {
        return file_exists($this->getLocalPath($this->file));
    }
    
    public function getArraysFromFile() {
        $html_source = Parcer::file_get_html($this->getLocalPath($this->file));
        $trs = $html_source->find('tr');
        $ind = self::FIRST_TR_INDEX;
        $return_array = [];
        $return_array['buy'] = [];
        $return_array['balance'] = [];
        $return_array['categoriesbuy'] = [];
        $return_array['categoriesbalance'] = [];
        $return_array['withBalance'] = "";
        $tmpval = 0;
        $tmpval2 = 0;
        var_dump($this->byTime);
        while (!isset($trs[$ind]->children[0]->attr['colspan'])||($trs[$ind]->children[0]->attr['colspan'] !== '10')) {
            if ($trs[$ind]->children[self::TD_OPER_INDEX]->innertext == self::TD_TITLE_BUY) {
                $x = ($this->byTime == true)?(int)strtotime(str_replace(".","-",$trs[$ind]->children[self::TD_TIME_INDEX]->innertext))*1000:$trs[$ind]->children[self::TD_TICKET_INDEX]->innertext;
                $tmpval = $tmpval + (float)str_replace(" ","",$trs[$ind]->children[self::TD_VALUE_INDEX]->innertext);
                if ($this->byTime == true){
                   array_push($return_array['buy'],['x' => $x,'y' => $tmpval]); 
                } else {
                   array_push($return_array['buy'],$tmpval);
                   array_push($return_array['categoriesbuy'],$x);
                }
                
            }
            
            
            if ($trs[$ind]->children[self::TD_OPER_INDEX]->innertext == self::TD_TITLE_BALANCE) {
                $x = ($this->byTime == true)?(int)strtotime(str_replace(".","-",$trs[$ind]->children[self::TD_TIME_INDEX]->innertext))*1000:$trs[$ind]->children[self::TD_TICKET_INDEX]->innertext;
                $tmpval2 = $tmpval2 + (float)str_replace(" ","",$trs[$ind]->children[self::TD_BALANCEVALUE_INDEX]->innertext);
                if ($this->byTime == true){
                   array_push($return_array['balance'],['x' => $x,'y' => $tmpval2]); 
                } else {
                   array_push($return_array['balance'],$tmpval2);
                   array_push($return_array['categoriesbalance'],$x);
                } 
                if ($this->withBalance) {
                    $x = ($this->byTime == true)?(int)strtotime(str_replace(".","-",$trs[$ind]->children[self::TD_TIME_INDEX]->innertext))*1000:$trs[$ind]->children[self::TD_TICKET_INDEX]->innertext;
                    $tmpval = $tmpval + (float)str_replace(" ","",$trs[$ind]->children[self::TD_BALANCEVALUE_INDEX]->innertext);
                    if ($this->byTime == true){
                       array_push($return_array['buy'],['x' => $x,'y' => $tmpval]); 
                    } else {
                       array_push($return_array['buy'],$tmpval);
                       array_push($return_array['categoriesbuy'],$x);
                    }
                }
                
            }
            
            $ind++;
        }
        array_push($return_array,['withBalance'=>$this->withBalance]);
        return($return_array);
    }
}
