<?php

use miloschuman\highcharts\Highcharts;

$this->title = 'Онлайн визуальный анализатор баланса ROBOFOREX (диаграмма)';

$this->params['breadcrumbs'][] = $this->title;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$nameChart1 = 'График "Изменение баланса"';
$nameChart2 = 'График "Изменение баланса прямыми операциями (пополнение/снятие)"';
$nameLine = "Операции покупки валюты";
if ($byTime) {
   $nameChart1 = $nameChart1." по времени";
   $nameChart2 = $nameChart2." по времени";
} else {
   $nameChart1 = $nameChart1." по операциям";
   $nameChart2 = $nameChart2." по операциям";   
}
if ($withBalance) {
   $nameChart1 = $nameChart1." (включая прямые операции (пополнение/снятие))";
   $nameLine = $nameLine." с прямыми операциями (пополнение/снятие)";
}
    echo Highcharts::widget([
        'setupOptions' => [
            'lang' => [
                'months' => ["Января","Февраля","Марта","Апреля","Мая","Июня","Июля", "Августа","Сентября","Октября","Ноября","Декабря"],
                'weekdays' => ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье'],
                'shortMonths' => ["Янв","Февр","Март","Апр","Май","Июнь","Июль", "Авг","Сент","Окт","Нояб","Дек"],
                'shortWeekdays' => ['Пон', 'Вт', 'Ср', 'Четв', 'Пятн', 'Суб', 'Воскр'],
            ]
        ],
        
        'options' => [
            'chart' => [
                'zoomType' => 'x'
            ],
           'title' => ['text' => $nameChart1],
           'xAxis' => [
               'type'=>($byTime)?'datetime':NULL,
               'categories' => ($byTime)?NULL:$data['categoriesbuy']
           ],
           'yAxis' => [
              'title' => ['text' => 'Изменение баланса']
           ],
           'series' => [
              ['type' =>'area','name' => $nameLine, 'data' => $data['buy']],
           ],
           'plotOptions' => [
                'area' => [
                    'fillColor' => [
                        'linearGradient' => [
                            'x1' => 0,
                            'y1' => 0,
                            'x2' => 0,
                            'y2' => 1
                        ],
                        'stops' => [
                            [0, "rgb(124, 181, 236)"],
                            [1, '#ffffff']
                        ]
                    ],
                    'marker' => [
                        'radius' => 2
                    ],
                    'lineWidth' => 1,
                    'states' => [
                        'hover' => [
                            'lineWidth' => 1
                        ]
                    ],
                    'threshold' => null
                ]
            ],
        ]
    ]);
if (!$withBalance) {
    echo "<hr style='border-top:1px solid black'/>";
    echo Highcharts::widget([
        'setupOptions' => [
            'lang' => [
                'months' => ["Января","Февраля","Марта","Апреля","Мая","Июня","Июля", "Августа","Сентября","Октября","Ноября","Декабря"],
                'weekdays' => ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье'],
                'shortMonths' => ["Янв","Февр","Март","Апр","Май","Июнь","Июль", "Авг","Сент","Окт","Нояб","Дек"],
                'shortWeekdays' => ['Пон', 'Вт', 'Ср', 'Четв', 'Пятн', 'Суб', 'Воскр'],
            ]
        ],
        
        'options' => [
            'chart' => [
                'zoomType' => 'x'
            ],
           'title' => ['text' => $nameChart2],
           'xAxis' => [
               'type'=>($byTime)?'datetime':NULL,
               'categories' => ($byTime)?NULL:$data['categoriesbalance']
           ],
           'yAxis' => [
              'title' => ['text' => 'Изменение баланса']
           ],
           'series' => [
              ['type' =>'area','name' => 'Операции пополнения и снятия', 'data' => $data['balance']],
           ],
           'plotOptions' => [
                'area' => [
                    'fillColor' => [
                        'linearGradient' => [
                            'x1' => 0,
                            'y1' => 0,
                            'x2' => 0,
                            'y2' => 1
                        ],
                        'stops' => [
                            [0, "rgb(124, 181, 236)"],
                            [1, '#ffffff']
                        ]
                    ],
                    'marker' => [
                        'radius' => 2
                    ],
                    'lineWidth' => 1,
                    'states' => [
                        'hover' => [
                            'lineWidth' => 1
                        ]
                    ],
                    'threshold' => null
                ]
            ],
        ]
    ]);    

}