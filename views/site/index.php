<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;



/* @var $this yii\web\View */

$this->title = 'Онлайн визуальный анализатор баланса ROBOFOREX';
?>
<h2 align="center">Онлайн визуальный анализатор баланса ROBOFOREX</h2><hr/>
<div class="primary-page">
<?php

    $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); 
    
    echo $form->field($modelStatistic, 'fileName', [
        'template' => '<div class="text-center">{label}{input}{error}{hint}</div>'
    ])->fileInput()->label("Файл html для анализа");
    echo $form->field($modelStatistic, 'withBalance')->checkbox(['value'=>'1','uncheck'=>'0'])->label("Включить в общий график прямые балансовые операции (пополнения/снятия)");  
    echo $form->field($modelStatistic, 'byTime')->checkbox(['value'=>'1','uncheck'=>'0'])->label("Зависимость от времени операции"); 
    echo Html::submitButton('Вывести график',['class'=>'btn btn-primary btn-outline','style'=>'display:block;margin:0 auto']);
      
    ActiveForm::end();
     
?>
</div>   
    

