<?php

namespace app\controllers;

use Yii;
use app\models\Statistic;
use yii\web\Controller;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $modelStatistic = new Statistic();
        if ($modelStatistic->load(Yii::$app->request->post())&&($modelStatistic->downloadFile())){
            return $this->redirect(['site/view',['file'=>$modelStatistic->tmpFileName,'byTime'=>$modelStatistic->byTime,'withBalance'=>$modelStatistic->withBalance]]);
        } else {
            return $this->render('index',['modelStatistic' => $modelStatistic]);
        }
    }
    
    public function actionView() 
    {   
        $modelStatistic = new Statistic();
        if (Yii::$app->request->get('1')['file']!==NULL) {
            $modelStatistic->file = Yii::$app->request->get('1')['file'];
            $modelStatistic->byTime = (Yii::$app->request->get('1')['byTime'] == '0')?false:true;
            $modelStatistic->withBalance = (Yii::$app->request->get('1')['withBalance'] == '0')?false:true;
            if ($modelStatistic->isExistsFile()) {
                $data = $modelStatistic->getArraysFromFile();
                return $this->render('view',['data' => $data,'byTime' => $modelStatistic->byTime,'withBalance'=>$modelStatistic->withBalance]);
            } else {
                return $this->render('index',['modelStatistic' => $modelStatistic]);
            }
        } else {
            return $this->render('index',['modelStatistic' => $modelStatistic]);
        }
    }

}
